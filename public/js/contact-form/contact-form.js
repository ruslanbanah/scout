var initContactForm = function(){
	// Get the form.
	var $formWrap = $('.form-content'),
			url = $formWrap.data('action-form');
	
	var renderForm = function(){
		if (url) {
			$.ajax({
				type: 'GET',
				url: url
			}).done(function(resp){
				$formWrap.html(resp);
			});
		}
	};
	
	var submitForm = function(event){
		event.preventDefault();
		var $form = $(event.currentTarget);
		var formData = $form.serialize();
		var result = function(resp){
			var $btnSubmit = $formWrap.html(resp).find('button[type="submit"]');
			$btnSubmit.prop('disabled', true);
			$btnSubmit.text('Sending...');
			setTimeout(function(){
				$btnSubmit.text('SUBMIT').prop('disabled', false);
			}, 1000)
		};
		
		// Submit the form using AJAX.
		$.ajax({
			type: 'POST',
			url: $form.attr('action'),
			data: formData
		}).done(result);
	};
	
	$formWrap.on('submit', 'form', submitForm);
	renderForm();
};

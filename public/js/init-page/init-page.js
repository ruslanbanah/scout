var initPage = function(){
	var vacItem = document.querySelectorAll(".tabs__row-item")
	var hamburger = document.querySelector("#showhide")
	var mobMenu = document.querySelector(".mobile-menu")
	var menuCloser = document.querySelector("#hideshow")
	var talkModalHamb = document.querySelector("#talkbutton")
	var talkModal = document.querySelector("#talkbutton1")
	var popup = document.querySelector(".popup-wrapper")
	var btnCloser = document.querySelector(".btn-close")
	var body = document.querySelector("body")
	var vacButton = document.querySelectorAll(".vacancy__btn")
	var vacancyItem = document.querySelectorAll(".vacancy__item")
	var vacReference = document.querySelectorAll('.vacancy__reference')
	vacanciesLviv = function () {
		vacancyItem.each(function (item) {
			if (item.dataset.city == "Lviv") {
				item.classList.remove('dis-none')
				item.classList.add('dis-flex')
			}
			else {
				item.classList.remove('dis-flex')
				item.classList.add('dis-none')
			}
		})
	}
	
	if (vacItem && vacItem.length > 0) {
		vacItem.each(function (item) {
			item.addEventListener('click', function (e) {
				var selected = this
				e.stopPropagation()
				vacItem.forEach(function (item) {
					if (item == selected) {
						item.classList.add("checked__tab")
					} else {
						item.classList.remove("checked__tab")
					}
				})
				if (e.target.id == "vacancies__Lviv") {
					console.log("we will show all the data from Lviv vacancies")
					vacanciesLviv()
				}
				else {
					console.log("we will show all the data from Bergen vacancies")
					vacanciesBergen()
				}
			})
		})
	}
	
	if (hamburger) {
		hamburger.addEventListener('click', function () {
			mobMenu.classList.add("maxheight")
			body.classList.add("nonscrollable")
		})
	}
	if (menuCloser) {
		menuCloser.addEventListener('click', function () {
			mobMenu.classList.remove("maxheight")
			body.classList.remove("nonscrollable")
		})
	}
	if (talkModalHamb) {
		talkModalHamb.addEventListener('click', function () {
			body.classList.add("nonscrollable")
			popup.classList.add("dis-block")
			popup.classList.add("maxheight")
		})
	}
	if (talkModal) {
		talkModal.addEventListener('click', function () {
			body.classList.add("nonscrollable")
			mobMenu.classList.remove("maxheight")
			popup.classList.add("dis-block")
			popup.classList.add("maxheight")
		})
	}
	if (btnCloser) {
		btnCloser.addEventListener('click', function () {
			popup.classList.remove("maxheight")
			popup.classList.remove("dis-block")
			body.classList.remove("nonscrollable")
		})
	}
	if (vacButton && vacButton.length) {
		vacButton.forEach(function (item) {
			item.addEventListener('click', function (e) {
				var a = document.querySelector('[data-id="' + e.target.dataset.btnid + '"]')
				console.log(a)
				var vacancyAbout = a.querySelector('.vacancy__about')
				var vacancyButton = a.querySelector('.vacancy__btn')
				var vacButtonCV = a.querySelector('.vacancy__btn-CV')
				var vacCloser = a.querySelector('.vacancy__reference-btn')
				var vacCloserCircle = a.querySelector('.vacancy__reference')
				vacancyAbout.style["max-height"] = "2000px"
				vacancyButton.classList.add('hidden')
				vacButtonCV.classList.add('visible')
				vacCloser.classList.add('visible')
				vacCloserCircle.classList.add('visible')
				vacCloserCircle.addEventListener('click', function (e) {
					vacancyAbout.style["max-height"] = "310px"
					vacButtonCV.classList.remove('visible')
					vacCloser.classList.remove('visible')
					vacCloserCircle.classList.remove('visible')
					vacancyButton.classList.remove('hidden')
					
				})
			})
		})
	}
};

document.addEventListener('DOMContentLoaded', function(){
	var approachWrap = document.getElementById('approach-wrap');
	var approachCanvas = document.getElementById('approach__content-canvas');
	
	initPage();
	initContactForm();
  lineBg(approachWrap, approachCanvas);
	scrollHash();
});

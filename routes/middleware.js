var _ = require('lodash');


/**
 Initialises the standard view locals
 */
exports.initLocals = function(req, res, next){
	res.locals.navLinks = [
		{label: 'What we do', key: 'what', href: '#what-we-do'},
		{label: 'Clients', key: 'client', href: '#clients'},
		{label: 'Meet the team', key: 'meet', href: '#meet-the-team'},
		{label: 'Career', key: 'career', href: '#career'},
		{label: 'Let`s Talk', key: 'talk', href: '/', talkBtn: 'talk'},
		// { label: 'admin', key: 'admin', href: '/keystone/signin' },
	];
	res.locals.user = req.user;
	next();
};


/**
 Fetches and clears the flashMessages before a view is rendered
 */
exports.flashMessages = function(req, res, next){
	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error'),
	};
	res.locals.messages = _.some(flashMessages, function(msgs){
		return msgs.length;
	}) ? flashMessages : false;
	next();
};


/**
 Prevents people from accessing protected pages when they're not signed in
 */
exports.requireUser = function(req, res, next){
	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/keystone/signin');
	} else {
		next();
	}
};

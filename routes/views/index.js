var keystone = require('keystone');

exports = module.exports = function(req, res){
	
	var view = new keystone.View(req, res);
	var locals = res.locals;
	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';
	locals.jumbotron = {
		title: 'Fantasy Solutions Provider',
		texts: [
			'Scout Gaming Group is Europes number one Fantasy software provider. ' +
			'We provide anything from small out-of-the-box solutions, to large customizable projects for Gaming/Betting-companies or Media. ' +
			'We have offices in Bergen/Norway, Stockholm/Sweden, Lviv/Ukraine, and Valetta/Malta. ' +
			'Currently our team consists of around 30 employees, from developers, designers to project managers & sales.',
			
			'Get in touch with us if you are interested to hear about tomorrows Fantasy Solutions for your business.'
		]
	};
	
	locals.approach = {
		title: 'Wide range of different sport, leagues and events',
		description: 'With our inhouse StatCenter we offer a wide range of playable sport and leagues, such as Football, Icehockey, Basketball,' +
		'Handball, Golf, Cycling, Olympic games, Poker and more. For football we currently offer 15 different leagues and cups, ' +
		'ranging from English Premier League to Chinese Premier League. Sport and leagues can be added on request. Stats you will ' +
		'receive are also custimizable, as well as rules & scoring. Suggestions for player-prices as well as different methods for ' +
		'picking players without budgets (tiers, for example) are provided. Player-statuses with information about injuries & ' +
		'suspensiens will make sure your customers are always up to date.'
	};
	
	// Render the view
	view.render('index', {layout: 'landing'});
};
